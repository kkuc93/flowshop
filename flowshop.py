# -*- coding: utf-8 -*-
"""
pip install pyschedule

for 4 machines and 6 times, solving takes 10 minutes
"""

#! /usr/bin/python
import pyschedule
import json

# Load config file
with open('config.json') as data_file:
    data = json.load(data_file)

time_limit = 600 #time limit set in seconds

# Validate config file
if 'machines' not in data:
     raise ValueError("Machines not defined in config file")

if 'timeLimit' in data:
    time_limit = data['timeLimit']

# Initialize proc table from config file
proc_table = []
for machine in data['machines']:
    proc_table.append(machine['task_durations'])

n = len(proc_table[0])
m = len(proc_table)

S = pyschedule.Scenario('Flowshop')
# Init tasks
T = { (i,j) : S.Task('T_%i_%i'%(i,j),length=proc_table[j][i]) for i in range(n) for j in range(m) }
# Init machines
R = { j : S.Resource(str(data['machines'][j]['name'])) for j in range(m) }

# Add constraint for order of execution (every task first have to run on first machine and so on)
S += [ T[i,j] < T[i,j+1] for i in range(n) for j in range(m-1) ]
# Add constraint rj
indices = [data['max_durations'].index(item) for item in data['max_durations'] if item != -1]
S += [ T[i,m-1] < data['max_durations'][i] for i in indices]
# Assing machines to tasks
for i in range(n) :
	for j in range(m) :
		T[i,j] += R[j]

# Set objective to minimum makespan
S.use_makespan_objective()
if pyschedule.solvers.mip.solve_bigm(S,time_limit=time_limit,msg=1):
    pyschedule.plotters.matplotlib.plot(S,resource_height=100.0,hide_tasks=[S._tasks['MakeSpan']])
else:
    print('no solution found')
